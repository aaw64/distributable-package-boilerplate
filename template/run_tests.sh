#!/bin/bash
echo "Running test suite with coverage report at the end"
echo -e "( would require coverage python package to be installed )\n"

# comma separated list of directories to omit from report
OMIT="{{app_name}}/tests/*,{{app_name}}/tests.py"
# comma separeted list of packages to monitor when coverage is run
SOURCE="{{app_name}}"

coverage run --source "$SOURCE" setup.py test
coverage report -m --omit "$OMIT"
